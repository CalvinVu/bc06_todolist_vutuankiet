import React, { Component } from "react";
import { Container } from "../../BaiTapStyledComponent/BaiTapToDoList/ComponentsToDoList/Container";
import { ThemeProvider } from "styled-components";
import { ToDoListDarkTheme } from "../Themes/ToDoListDarkTheme";
import { ToDoListLightTheme } from "../Themes/ToDoListLightTheme";
import { ToDoListPrimaryTheme } from "../Themes/ToDoListPrimaryTheme";
import { Dropdown } from "../BaiTapToDoList/ComponentsToDoList/Dropdown";
import { arrayTheme } from "../Themes/ThemeManager";

import {
  Heading1,
  Heading2,
  Heading3,
  Heading4,
  Heading5,
} from "../BaiTapToDoList/ComponentsToDoList/Heading";
import { Label, TextField, Input } from "./ComponentsToDoList/TextField";
import { Button } from "../BaiTapToDoList/ComponentsToDoList/Button";
import {
  Table,
  Thead,
  Tbody,
  Tr,
  Td,
  Th,
} from "../BaiTapToDoList/ComponentsToDoList/Table";
import { connect } from "react-redux";
import {
  addTaskAction,
  changeThemeAction,
  doneTaskAction,
  deleteTaskAction,
  editTaskAction,
  updateTask,
} from "../../redux/actions/ToDoListAction";

class ToDoList extends Component {
  state = {
    taskName: "",
    disabled: true,
  };
  renderTaskToDo = () => {
    return this.props.taskList
      .filter((task) => !task.done)
      .map((task, index) => {
        return (
          <Tr key={index}>
            <Th valign="middle" className="text-left">
              {task.taskName}
            </Th>
            <Th className="text-right">
              <Button
                onClick={() => {
                  this.setState({ disabled: false }, () => {
                    this.props.dispatch(editTaskAction(task));
                  });
                }}
              >
                <i className="fa fa-check">Write</i>
              </Button>
              <Button
                onClick={() => {
                  this.props.dispatch(doneTaskAction(task.id));
                }}
              >
                <i className="fa fa-check">Done</i>
              </Button>
              <Button
                onClick={() => {
                  this.props.dispatch(deleteTaskAction(task.id));
                }}
              >
                <i className="fa fa-trash">Delete</i>
              </Button>
            </Th>
          </Tr>
        );
      });
  };
  renderTaskCompleted = () => {
    return this.props.taskList
      .filter((task) => task.done)
      .map((task, index) => {
        return (
          <Tr key={index}>
            <Th valign="middle" className="text-left">
              {task.taskName}
            </Th>
            <Th className="text-right">
              <Button
                onClick={() => {
                  this.props.dispatch(deleteTaskAction(task.id));
                }}
              >
                <i className="fa fa-trash">Delete</i>
              </Button>
            </Th>
          </Tr>
        );
      });
  };

  // handleChange = (e) => {
  //   let{name,value} = e.target.value;
  //   this.setState({
  //     [name]: value
  //   })
  // }

  // viet ham render theme import theme manager
  renderTheme = () => {
    return arrayTheme.map((theme) => {
      return <option value={theme.id}>{theme.name}</option>;
    });
  };

  componentWillReceiveProps(newProps) {
    console.log(this.props);
    console.log(newProps);
    this.setState({
      taskName: newProps.taskEdit.taskName,
    });
  }
  render() {
    return (
      <ThemeProvider theme={this.props.themeToDoList}>
        <Container className="w-50">
          <Dropdown
            onChange={(e) => {
              let { value } = e.target;
              console.log(e.target);
              // dispatch value len reducer
              this.props.dispatch(changeThemeAction(value));
            }}
          >
            {this.renderTheme()}
          </Dropdown>
          <Heading3>To do list</Heading3>
          <TextField
            label="task name "
            className="w-50"
            value={this.state.taskName}
            onChange={(e) => {
              this.setState(
                {
                  taskName: e.target.value,
                },
                () => {
                  console.log(this.state);
                }
              );
            }}
          ></TextField>
          <Button
            className="ml-2"
            onClick={() => {
              //lấy thông tin người dùng nhập vào từ input
              let { taskName } = this.state;
              //tạo ra 1 task object
              let newTask = {
                id: Date.now(),
                taskName: taskName,
                done: false,
              };
              console.log(newTask);
              //đưa task object lên rudex thông qua phương thức díspath
              this.props.dispatch(addTaskAction(newTask));
            }}
          >
            <i className="fa fa-plus"></i>
            Add task
          </Button>
          <br />
          {this.state.disabled ? (
            <Button
              disabled
              className="ml-2"
              onClick={() => {
                this.props.dispatch(updateTask(this.state.taskName));
              }}
            >
              <i className="fa fa-upload"></i> Update task
            </Button>
          ) : (
            <Button
              className="ml-2"
              onClick={() => {
                let { taskName } = this.state;
                this.setState(
                  {
                    disabled: true,
                    taskName: " ",
                  },
                  () => {
                    this.props.dispatch(updateTask(taskName));
                  }
                );
              }}
            >
              <i className="fa fa-upload"></i> Update task
            </Button>
          )}

          <hr />
          <Heading3>Task to do</Heading3>
          <Table>
            <Thead>{this.renderTaskToDo()}</Thead>
          </Table>
          <Heading3>Task compied</Heading3>
          <Table>
            <Thead>{this.renderTaskCompleted()}</Thead>
          </Table>
        </Container>
      </ThemeProvider>
    );
  }

  componentDidUpdate(prevProps, prevState) {
    // so sanh neu nhu props truoc do (taskEdit truoc ma khac taskEdit hien tai thi minh moi setState)
    if (prevProps.taskEdit.id !== this.props.taskEdit.id) {
      this.setState({
        taskName: this.props.taskEdit.taskName,
      });
    }
  }
}

const mapStateToProps = (state) => {
  return {
    themeToDoList: state.ToDoListReducer.themeToDoList,
    taskList: state.ToDoListReducer.taskList,
    taskEdit: state.ToDoListReducer.taskEdit,
  };
};

export default connect(mapStateToProps)(ToDoList);
