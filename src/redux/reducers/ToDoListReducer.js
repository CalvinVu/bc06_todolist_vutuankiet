import { ToDoListDarkTheme } from "../../BaiTapStyledComponent/Themes/ToDoListDarkTheme";
import {
  add_task,
  delete_task,
  done_task,
  edit_task,
  update_task,
} from "../types/ToDoListTypes";
import { arrayTheme } from "../../BaiTapStyledComponent/Themes/ThemeManager";

const initialState = {
  themeToDoList: ToDoListDarkTheme,
  taskList: [
    { id: "task-1", taskName: "task 1", done: true },
    { id: "task-2", taskName: "task 2", done: false },
    { id: "task-3", taskName: "task 3", done: true },
    { id: "task-4", taskName: "task 4", done: false },
  ],
  taskEdit: { id: "task-1", taskName: "task 1", done: false },
};

export default (state = initialState, action) => {
  switch (action.type) {
    case add_task: {
      //kiem tra rong
      if (action.newTask.taskName.trim() === "") {
        alert("taskname is required");
        return { ...state };
      }
      // kiem tra ton tai
      let taskListUpdate = [...state.taskList];
      let index = taskListUpdate.findIndex(
        (task) => task.taskName === action.newTask.taskName
      );
      if (index !== -1) {
        alert("taskname already exists");
        return { ...state };
      }
      taskListUpdate.push(action.newTask);
      // xu ly xong thi gan taskList moi vao taskList
      state.taskList = taskListUpdate;
    }
    case "change_theme": {
      console.log(action);
      // tim theme qua id
      let theme = arrayTheme.find((theme) => theme.id == action.themeId);
      console.log(theme);
      if (theme) {
        state.themeToDoList = { ...theme.theme };
      }
      return { ...state };
    }
    case done_task: {
      //click  vao button check => dispatch len action co taskID
      console.log("done_task", action);
      let taskListUpdate = [...state.taskList];
      // tu task tim ra task do o vi tri nao trong mang, tien hanh cap nhat lai thuoc tinh done = true va cap nhat lai state cua redux
      let index = taskListUpdate.findIndex((task) => task.id === action.taskId);
      console.log(index);
      if (index !== -1) {
        taskListUpdate[index].done = true;
      }
      // state.taskList = taskListUpdate
      return { ...state, taskList: taskListUpdate };
    }
    case delete_task: {
      console.log(action);
      let taskListUpdate = [...state.taskList];
      taskListUpdate = taskListUpdate.filter(
        (task) => task.id !== action.taskId
      );
      return { ...state, taskList: taskListUpdate };
    }
    case edit_task: {
      return { ...state, taskEdit: action.task };
    }
    case update_task: {
      // chinh sua lai task name cua task edit
      state.taskEdit = { ...state.taskEdit, taskName: action.taskName };
      //tim trong tasklist cap nhat lai taskedit nguoi dung update
      let taskListUpdate = [...state.taskList];
      let index = taskListUpdate.findIndex(
        (task) => task.id === state.taskEdit.id
      );
      if (index !== -1) {
        taskListUpdate[index] = state.taskEdit;
      }
      state.taskList = taskListUpdate;
      return { ...state };
    }
    default:
      return { ...state };
  }
};
